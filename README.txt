Login as Employee
	Email:imagine@beatles.com
	Password:LetItBe
Login as Manager
	Email:troy@fakeemail
	Password:1234

Tutorials
Employee and Manager
	Submit Request:
		Input an amount and reason for reimbursment and hit submit
	View Pending:
		Will automatically pull up all the users pending requests
	View Resolved:
		Will automatically pull up all the users resolved requests

Manager
	View Pending Employee:
		Will automatically pull up all pending requests in the database for approval.  Will not show requests made by the current user so they can't approve their own.

Database Structure:
	The database consisits of 2 tables
	USERS:
		Stores user information
		Has u_id,name,email,password,status
		u_id is the primary key
		name,email, and password have the not null constraint
		status has a check constraint so only 'EMP' or 'MGMT' values are allowed
	RECORDS:
		Stores reimbursement request information
		Has r_id,u_id,amount,reason,status
		r_id is the primary key
		u_id is a foreign key to the users tables
		amount and reason have the not null constraint
		status has a check constraint of 'W','Y','N'.  By default all new requests are 'W' for waiting
		
	