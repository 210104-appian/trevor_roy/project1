document.getElementById("login_btn").addEventListener("click", login);

function login(){

    let username = document.getElementById("uname").value;
    let password = document.getElementById("password").value;

    let user = {email:username, password :password};
    const userJson = JSON.stringify(user);
    console.log(user);
    console.log(userJson);
    performAjaxPostRequest(baseUrl+"login",userJson,handleSuccessfulLogin,handleUnsuccessfulLogin);
    console.log("here");


}

function handleSuccessfulLogin(responseText) {
    console.log("Success! You're logged in");
    //document.getElementById("error-msg").hidden = true;
    //closeModal();
    let token = responseText;
    
    console.log(token);
    token = JSON.parse(token);
    console.log(token);
    sessionStorage.setItem("userId", token.uId);
    sessionStorage.setItem("userName",token.name);
    if(token.status == "MGMT"){
      window.location.href = otherBase + "ManagerHomePage.html"
    }else{
      window.location.href = otherBase + "EmployeeHomePage.html"
    }
    //toggleLoginToLogout();
    //displayLoggedInUser();
  }
  
  function handleUnsuccessfulLogin() {
    console.log("Login unsuccessful");
    //document.getElementById("error-msg").hidden = false;
  }
  
  function toggleLoginToLogout() {
    let loginBtn = document.getElementById("login-nav-opt");
    loginBtn.innerText = "Log out";
    loginBtn.removeEventListener("click", openModal);
    loginBtn.addEventListener("click", logout);
  }
  
  function logout() {
    sessionStorage.removeItem("token");
    toggleLogoutToLogin();
    removeLoggedInUserGreeting();
  }
  
  function toggleLogoutToLogin() {
    let loginBtn = document.getElementById("login-nav-opt");
    loginBtn.innerText = "Log in";
    loginBtn.addEventListener("click", openModal);
    loginBtn.removeEventListener("click", logout);
  }

