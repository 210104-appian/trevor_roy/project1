const baseUrl = "http://localhost:8080/project-1/"
const otherBase = "file:///C:/Users/roy_t/Documents/workspace-spring-tool-suite-4-4.9.0.RELEASE/project-1/src/main/webapp/static/"

function performAjaxPostRequest(url, payload, successCallback, failureCallback){
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.onreadystatechange = function(){
        if(xhr.readyState==4 ){
            if(xhr.status>199 && xhr.status<300){
                successCallback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                if(failureCallback){
                    failureCallback()
                } else{
                    console.error("An error occurred while attempting to create a new record")
                }
            }
        }
    }
    xhr.send(payload);
}

function performAjaxGetRequest(url,successCallback){
    console.log(url);
    const xhr = new XMLHttpRequest();
    xhr.open("GET",url);
    xhr.onreadystatechange = function(){
        console.log("1");
        if(xhr.readyState==4 ){
            console.log("2");
            if(xhr.status>199 && xhr.status<300){
                console.log("3");
                successCallback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                console.log("4");
                if(failureCallback){
                    failureCallback()
                } else{
                    console.error("An error occurred while attempting to create a new record")
                }
            }
        }
    }
    xhr.send();

}

function closeSubmit(){
    subModal.style.display = "none";
}

function closePending(){
    document.getElementById("view-pending-modal").style.display = "none";
    //let rows = document.getElementsByClassName("pendingRow");
    var row = document.getElementsByTagName('tbody')[0];
    row.parentNode.removeChild(row);  
    document.getElementById("pending-error").hidden = true; 
}

function closeResolved(){
    document.getElementById("view-resolved-modal").style.display = "none";
    //let rows = document.getElementsByClassName("resolvedRow");
    var row = document.getElementsByTagName('tbody')[0];
    row.parentNode.removeChild(row);
    document.getElementById("resolved-error").hidden = true;  
}

function closeEmp(){
    document.getElementById("approvedeny-modal").style.display = "none";
    let rows = document.getElementsByClassName("adRow");
    var row = document.getElementsByTagName('tbody')[0];
    row.parentNode.removeChild(row);
}