const subModal = document.getElementById("submitModal");
const pendingTable = document.getElementById("pendingTable");
const resolvedTable = document.getElementById("resolvedTable");
let uId = sessionStorage.getItem("userId");

//Click on submit button
document.getElementById("submit").addEventListener("click",openSubmit);
document.getElementById("submit").addEventListener("click",closePending);
document.getElementById("submit").addEventListener("click",closeResolved);
document.getElementById("submit").addEventListener("click",closeEmp);

document.getElementById("submit-btn").addEventListener("click",submit);

document.getElementById("close-btn").addEventListener("click",closeSubmit);

//click on pending
document.getElementById("viewP").addEventListener("click",viewPending);
document.getElementById("viewP").addEventListener("click",closeSubmit);
document.getElementById("viewP").addEventListener("click",closeResolved);
document.getElementById("viewP").addEventListener("click",closeEmp);

//click on resolved
document.getElementById("viewR").addEventListener("click",viewResolved);
document.getElementById("viewR").addEventListener("click",closePending);
document.getElementById("viewR").addEventListener("click",closeSubmit);
document.getElementById("viewR").addEventListener("click",closeEmp);

//click logout
document.getElementById("logout").addEventListener("click",logout);

document.getElementById("pendingClose").addEventListener("click",closePending);

document.getElementById("resolvedClose").addEventListener("click",closeResolved);

function submit(){
    let amount = document.getElementById("amount-input").value;
    let reason = document.getElementById("reason-input").value;
    
    console.log(sessionStorage.getItem("userId") + " " + sessionStorage.getItem("userName"));
    let submission = {uId, amount,reason};
    const subJson = JSON.stringify(submission);
    console.log(subJson);
    performAjaxPostRequest(baseUrl+"submit",subJson,handleSuccessfulRequest,handleUnsuccessfulRequest);
    console.log("hello");



}

function handleSuccessfulRequest(){
    document.getElementById("error-msg").hidden = true;
    document.getElementById("success-msg").hidden = false;
}

function handleUnsuccessfulRequest(){
    document.getElementById("success-msg").hidden = true;
    document.getElementById("error-msg").hidden = false;
}

function openSubmit(){
    subModal.style.display = "block";
}







function viewPending(){
    let user = {uId};
    const userJson = JSON.stringify(user);  
    document.getElementById("view-pending-modal").style.display = "block";
    console.log(userJson);
    performAjaxPostRequest(baseUrl+"pending",userJson,handleSuccessfulViewing,handleUnsuccessfulViewing);
}

function viewResolved(){
    let user = {uId};
    const userJson = JSON.stringify(user);
    document.getElementById("view-resolved-modal").style.display = "block";
    performAjaxPostRequest(baseUrl+"resolved",userJson,handleSuccessfulViewingResolved,hanldeUnsuccessfulResolved);

}

function handleSuccessfulViewing(responseText){
    console.log("PENDING COMPLETE");
    console.log(responseText);
    let token = responseText;
    let string = token.split("ttt");
    console.log(string.length);
    var newRow = pendingTable.insertRow();
    newRow.setAttribute("class","pendingRow")
    var cell1 = newRow.insertCell(0);
    var cell2 = newRow.insertCell(1);
    var cell3 = newRow.insertCell(2);
    cell1.innerHTML = "RecordId";
    cell2.innerHTML = "Amount";
    cell3.innerHTML = "Reason";
    string.forEach(parsingJsonPend);
        
}

function handleSuccessfulViewingResolved(responseText){
    console.log("PENDING COMPLETE");
    console.log(responseText);
    let token = responseText;
    let string = token.split("ttt");
    console.log(string.length);
    var newRow = resolvedTable.insertRow();
    newRow.setAttribute("class","resolvedRow")
    var cell1 = newRow.insertCell(0);
    var cell2 = newRow.insertCell(1);
    var cell3 = newRow.insertCell(2);
    var cell4 = newRow.insertCell(3);
    cell1.innerHTML = "RecordId";
    cell2.innerHTML = "Amount";
    cell3.innerHTML = "Reason";
    cell4.innerHTML = "Status"
    string.forEach(parsingJsonRes);
        
}


function handleUnsuccessfulViewing(){
    console.log("PENDING NOT DONE");
    document.getElementById("pending-error").hidden = false;
}

function hanldeUnsuccessfulResolved(){
    document.getElementById("resolved-error").hidden = false;
}

function parsingJsonPend(item){
        console.log(item);
        item = JSON.parse(item);
        console.log(item)
        var newRow =pendingTable.insertRow();
        newRow.setAttribute("class","pendingRow")
        var cell1 = newRow.insertCell(0);
        var cell2 = newRow.insertCell(1);
        var cell3 = newRow.insertCell(2);
        cell1.innerHTML = item.rId;
        cell2.innerHTML = item.amount;
        cell3.innerHTML = item.reason;
}

function parsingJsonRes(item){
    console.log(item);
    item = JSON.parse(item);
    console.log(item)
    var newRow =resolvedTable.insertRow();
    newRow.setAttribute("class","resolvedRow")
    var cell1 = newRow.insertCell(0);
    var cell2 = newRow.insertCell(1);
    var cell3 = newRow.insertCell(2);
    var cell4 = newRow.insertCell(3);
    cell1.innerHTML = item.rId;
    cell2.innerHTML = item.amount;
    cell3.innerHTML = item.reason;
    if(item.status == "Y"){
        cell4.innerHTML = "A";
    }else{
        cell4.innerHTML = "D"
    }
    
}

function logout(){
    sessionStorage.clear();
    window.location.href = otherBase + "landingpage.html"
}