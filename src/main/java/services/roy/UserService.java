package services.roy;



import java.sql.ResultSet;

import daos.roy.UserDao;
import daos.roy.UserDaoImpl;
import models.roy.User;

public class UserService {
	private UserDao user = new UserDaoImpl();
	
	public User userLogin(String uName,String password) {	
		return user.userLogin(uName, password);
		
	}

}
