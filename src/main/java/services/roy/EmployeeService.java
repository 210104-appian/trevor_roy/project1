package services.roy;

import java.sql.ResultSet;
import java.util.List;

import daos.roy.ReimbursmentDao;
import daos.roy.ReimbursmentDaoImpl;
import models.roy.ReimbursementRecord;

public class EmployeeService {
	private ReimbursmentDao rd = new ReimbursmentDaoImpl();
	
	public void submit(int uId,double amount, String reason) {
		rd.submit(uId, amount, reason);
	}
	
	public List<ReimbursementRecord> viewPending(int uId) {
		return rd.viewPending(uId);
	}
	
	public List<ReimbursementRecord> viewResolved(int uId){
		return rd.viewResolved(uId);
	}

}
