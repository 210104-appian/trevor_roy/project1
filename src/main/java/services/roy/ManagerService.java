package services.roy;

import java.util.List;

import daos.roy.ReimbursmentDaoImpl;
import models.roy.ReimbursementRecord;

public class ManagerService {
	ReimbursmentDaoImpl rDao= new ReimbursmentDaoImpl(); 
	public void approveDeny(int rId, String choice) {
		rDao.approveDeny(rId, choice);
	}
	public List<ReimbursementRecord> viewPending(){
		return rDao.manViewPending();
	}

}
