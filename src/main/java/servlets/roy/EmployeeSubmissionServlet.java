package servlets.roy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import models.roy.ReimbursementRecord;
import services.roy.EmployeeService;

public class EmployeeSubmissionServlet extends HttpServlet {
	
	EmployeeService ess = new EmployeeService();
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("Got a post request REQUESTREIMBURSMENT");
		ObjectMapper om = new ObjectMapper();
		ReimbursementRecord rr = om.readValue(request.getReader().readLine(), ReimbursementRecord.class);
		ess.submit(rr.getuId(),rr.getAmount(),rr.getReason());
	}

}
