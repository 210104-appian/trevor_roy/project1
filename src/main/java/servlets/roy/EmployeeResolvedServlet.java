package servlets.roy;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import models.roy.ReimbursementRecord;
import models.roy.User;
import services.roy.EmployeeService;

public class EmployeeResolvedServlet extends HttpServlet {
	EmployeeService ess = new EmployeeService();
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("Got a post request VIEWRESOLVED");
		ObjectMapper om = new ObjectMapper();
		System.out.println("1");
		User user = om.readValue(request.getReader().readLine(), User.class);
		System.out.println("2");
		List<ReimbursementRecord> records = ess.viewResolved(user.getuId());
		System.out.println("3");
		
		String jsonRecords ="";
		if(records != null) {
			System.out.println("HERE!");
			for(ReimbursementRecord record:records) {
				//System.out.println(record.toString());
				jsonRecords = jsonRecords + om.writerWithDefaultPrettyPrinter().writeValueAsString(record)+ "ttt";
				
			}
			jsonRecords = jsonRecords.substring(0,jsonRecords.length()-3);
			try(PrintWriter pw = response.getWriter()){
				pw.write(jsonRecords);
		
			}
		}else {
			System.out.println("FAILURE");
			response.sendError(401);
		}
	}
	
}
