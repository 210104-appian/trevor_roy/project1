package servlets.roy;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import models.roy.ReimbursementRecord;
import models.roy.User;
import services.roy.EmployeeService;

public class EmployeePendingServlet extends HttpServlet{
	
	EmployeeService ess = new EmployeeService();
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("Got a post request VIEWPENDING");
		ObjectMapper om = new ObjectMapper();
		User user = om.readValue(request.getReader().readLine(), User.class);
		System.out.println(user.toString());
		List<ReimbursementRecord> records = ess.viewPending(user.getuId());

		String jsonRecords ="";
		if(records != null) {
			System.out.println("HERE!");
			for(ReimbursementRecord record:records) {
				//System.out.println(record.toString());
				jsonRecords = jsonRecords + om.writerWithDefaultPrettyPrinter().writeValueAsString(record)+ "ttt";
				
			}
			jsonRecords = jsonRecords.substring(0,jsonRecords.length()-3);
			try(PrintWriter pw = response.getWriter()){
				pw.write(jsonRecords);
		
			}
		}else {
			response.sendError(401);
		}
		
		
	}

}
