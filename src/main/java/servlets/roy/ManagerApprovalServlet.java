package servlets.roy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import models.roy.ApprovalDenied;
import services.roy.ManagerService;

public class ManagerApprovalServlet extends HttpServlet {
	
	ManagerService ms = new ManagerService();
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("Get request on ManagerApproval");
		ObjectMapper om = new ObjectMapper();
		System.out.println("HERE WE GO");
		System.out.println("HERE I AM");
		ApprovalDenied ad = om.readValue(request.getReader().readLine(), ApprovalDenied.class);
		System.out.println(ad.toString());
		ms.approveDeny(ad.getrId(), ad.getChoice());
		
	}

}
