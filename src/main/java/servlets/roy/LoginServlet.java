package servlets.roy;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import models.roy.User;
import services.roy.UserService;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserService us = new UserService();
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		System.out.println("Got a post request LOGIN");
		ObjectMapper om = new ObjectMapper();
		User credentials = om.readValue(request.getReader().readLine(), User.class);
		User user = us.userLogin(credentials.getEmail(), credentials.getPassword());
		
		if(user.getuId() != 0) {
			String jsonUser = om.writerWithDefaultPrettyPrinter().writeValueAsString(user);
			System.out.println(jsonUser);
			try(PrintWriter pw = response.getWriter()){
				pw.write(jsonUser);
			}
		} else {
			response.sendError(401);
		}
	}

}
