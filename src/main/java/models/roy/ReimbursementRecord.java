package models.roy;

public class ReimbursementRecord {
	protected int rId;
	protected int uId;
	protected double amount;
	protected String reason;
	protected String status;
	
	public ReimbursementRecord(int rId, int uId, double amount, String reason,String status) {
		super();
		this.rId = rId;
		this.uId = uId;
		this.amount = amount;
		this.reason = reason;
		this.status = status;
	}
	
	
	
	public ReimbursementRecord() {
		super();
	}



	public ReimbursementRecord(int uId, double amount, String reason) {
		super();
		this.uId = uId;
		this.amount = amount;
		this.reason = reason;
	}



	public int getrId() {
		return rId;
	}
	public void setrId(int rId) {
		this.rId = rId;
	}
	public int getuId() {
		return uId;
	}
	public void setuId(int uId) {
		this.uId = uId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	@Override
	public String toString() {
		return "ReimbursementRecord [rId=" + rId + ", uId=" + uId + ", amount=" + amount + ", reason=" + reason
				+ ", status=" + status + "]";
	}
	
	
	

}
