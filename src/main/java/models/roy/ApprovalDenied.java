package models.roy;

public class ApprovalDenied {
	protected int rId;
	protected String choice;
	public ApprovalDenied() {
		super();
	}
	public ApprovalDenied(int rId, String choice) {
		super();
		this.rId = rId;
		this.choice = choice;
	}
	public int getrId() {
		return rId;
	}
	public void setrId(int rId) {
		this.rId = rId;
	}
	public String getChoice() {
		return choice;
	}
	public void setChoice(String choice) {
		this.choice = choice;
	}
	@Override
	public String toString() {
		return "ApprovalDenied [rId=" + rId + ", choice=" + choice + "]";
	}
	

}
