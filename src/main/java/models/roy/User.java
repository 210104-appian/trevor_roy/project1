package models.roy;

public class User {
	protected int uId;
	protected String name;
	protected String email;
	protected String password;
	protected String status;
	public User(int uId, String name, String email, String password, String status) {
		super();
		this.uId = uId;
		this.name = name;
		this.email = email;
		this.password = password;
		this.status = status;
	}
	public User() {
		super();
	}
	public int getuId() {
		return uId;
	}
	public void setuId(int uId) {
		this.uId = uId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "User [uId=" + uId + ", name=" + name + ", email=" + email + ", password=" + password + ", status="
				+ status + "]";
	}
	
	

}
