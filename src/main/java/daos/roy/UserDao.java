package daos.roy;

import java.sql.ResultSet;

import models.roy.User;

public interface UserDao {
	
	public User userLogin(String username, String password);

}
