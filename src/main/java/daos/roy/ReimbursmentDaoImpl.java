package daos.roy;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import models.roy.ReimbursementRecord;
import util.roy.ConnectionUtil;

public class ReimbursmentDaoImpl implements ReimbursmentDao {

	private static Logger log = Logger.getRootLogger();
	@Override
	public void submit(int uId, double amount, String reason) {
		try(Connection connection = ConnectionUtil.getConnection();){
			log.info("INERTING into RECORDS");
			PreparedStatement pState = connection.prepareStatement("INSERT INTO RECORDS(U_ID,AMOUNT,REASON,STATUS) VALUES(?,?,?,'W')");
			pState.setInt(1, uId);
			pState.setDouble(2, amount);
			pState.setString(3, reason);
			pState.executeUpdate();
			log.info("INSERT complete");
		}catch(SQLException e) {
			e.printStackTrace();
			//return null;
		
		}

	}
	
	public List<ReimbursementRecord> viewPending(int uId) {
		try(Connection connection = ConnectionUtil.getConnection();){
			List<ReimbursementRecord> records = new ArrayList<>();
			log.info("SELECTING from RECORDS");
			PreparedStatement pState = connection.prepareStatement("SELECT R_ID, AMOUNT, REASON FROM RECORDS WHERE U_ID = ? AND STATUS = 'W'");
			pState.setInt(1, uId);
			ResultSet rs = pState.executeQuery();
			log.info("SELECT complete");
			while(rs.next()) {
				ReimbursementRecord rr = new ReimbursementRecord();
				rr.setrId(rs.getInt(1));
				rr.setAmount(rs.getDouble(2));
				rr.setReason(rs.getString(3));
				records.add(rr);
			}
			return records;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
		
		
	}

	@Override
	public List<ReimbursementRecord> viewResolved(int uId) {
		try(Connection connection = ConnectionUtil.getConnection();){
			List<ReimbursementRecord> records = new ArrayList<>();
			log.info("SELECTING from RECORDS");
			PreparedStatement pState = connection.prepareStatement("SELECT R_ID, AMOUNT, REASON,STATUS FROM RECORDS WHERE U_ID = ? AND STATUS != 'W'");
			pState.setInt(1, uId);
			ResultSet rs = pState.executeQuery();
			log.info("SELECTING COMPLETE");
			while(rs.next()) {
				ReimbursementRecord rr = new ReimbursementRecord();
				rr.setrId(rs.getInt(1));
				rr.setAmount(rs.getDouble(2));
				rr.setReason(rs.getString(3));
				rr.setStatus(rs.getString(4));
				records.add(rr);
			}
			return records;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void approveDeny(int rId, String choice) {
		try(Connection connection = ConnectionUtil.getConnection();){
			//PreparedStatement pState;
			if(choice.equals("A")) {
				System.out.println("approved");
				log.info("UPDATING record:" + rId + " in RECORDS");
				PreparedStatement pState = connection.prepareStatement("UPDATE RECORDS SET STATUS = 'Y' WHERE R_ID = ?");
				
					pState.setInt(1, rId);
					pState.executeUpdate();
					
				
				
			}else if (choice.equals("D")) {
				System.out.println("denied");
				log.info("UPDATING record:" + rId + " in RECORDS");
				PreparedStatement pState = connection.prepareStatement("UPDATE RECORDS SET STATUS = 'N' WHERE R_ID = ?");
					pState.setInt(1, rId);
					pState.executeUpdate();
					
				
			}else {
				System.out.println("NOPE");
			}
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public List<ReimbursementRecord> manViewPending() {
		try(Connection connection = ConnectionUtil.getConnection();){
			List<ReimbursementRecord> records = new ArrayList<>();
			log.info("SELECTING from RECORDS");
			PreparedStatement pState = connection.prepareStatement("SELECT R_ID, U_ID, AMOUNT, REASON FROM RECORDS WHERE STATUS = 'W'");
			ResultSet rs = pState.executeQuery();
			log.info("SELECTING complete");
			while(rs.next()) {
				ReimbursementRecord rr = new ReimbursementRecord();
				rr.setrId(rs.getInt(1));
				rr.setuId(rs.getInt(2));
				rr.setAmount(rs.getDouble(3));
				rr.setReason(rs.getString(4));
				records.add(rr);
			}
			return records;
		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
