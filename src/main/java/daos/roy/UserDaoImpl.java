package daos.roy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import models.roy.User;
import util.roy.ConnectionUtil;

public class UserDaoImpl implements UserDao {
	private static Logger log = Logger.getRootLogger();
	public User userLogin(String username, String password) {
		try(Connection connection = ConnectionUtil.getConnection();){
			User user = new User();
			log.info("SELECTING from RECORDS");
			PreparedStatement pstate = connection.prepareStatement("SELECT U_ID,NAME, STATUS FROM USERS WHERE EMAIL = ? AND PASSWORD = ?");
			pstate.setString(1, username);
			pstate.setString(2, password);
			ResultSet rs = pstate.executeQuery();
			log.info("SELECTING complete");
			while(rs.next()) {
				user.setuId(rs.getInt(1));
				user.setName(rs.getString(2));
				user.setStatus(rs.getString(3));
				System.out.println(rs.getInt(1));
				System.out.println(rs.getString(2));
				System.out.println(rs.getString(3));
			}
			return user;

		}catch(SQLException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	

}
