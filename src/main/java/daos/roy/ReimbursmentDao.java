package daos.roy;

import java.util.List;

import models.roy.ReimbursementRecord;

public interface ReimbursmentDao {
	
	public void submit(int uId, double amount, String reason);
	public List<ReimbursementRecord> viewPending(int uId);
	public List<ReimbursementRecord> viewResolved(int uId);
	public void approveDeny(int rId,String choice);
	public List<ReimbursementRecord> manViewPending();

}
